const schedule = require("node-schedule");
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime')
dayjs.extend(relativeTime)

console.log("And it's one more beer and I don't hear you anymore...");

//making sure npm run develop works
if (process.env.NODE_ENV === "develop") {
  console.log('👨‍💻 Development mode')
  require("dotenv").config();
};

const web = require('./web');
const twitter = require('./lib/twitter');


// Schedule
var rule = new schedule.RecurrenceRule();
rule.hour = 4;
rule.minute = 0;
rule.second = 0;
rule.tz = "Etc/GMT";

function checkOnJob() {
  console.log(`Next job ${dayjs(job.nextInvocation()).fromNow()}`)
}

//sendTweet()
const job = schedule.scheduleJob(rule, twitter.sendTweet);
const check = schedule.scheduleJob('*/5 * * * *', checkOnJob);

checkOnJob();
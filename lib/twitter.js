const Twit = require('twit');
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime')
const utc = require('dayjs/plugin/utc')
dayjs.extend(relativeTime)
dayjs.extend(utc)

const phrases = require('./phrases')
const config = require('../config');

const T = new Twit(config);

function sendTweet() {
    console.log(phrases.main);

    T.post('statuses/update', { status: phrases.main })
        .then(val => console.log('Posted', val.data))
        .catch(err => console.error(err))
}

function saveLife(save, handle) {
    let status = save ? phrases.saved : 'No one saved my life';
    if (handle) handle = handle.includes('@') ? handle : '@' + handle
    if (handle && save) status = `${status} \n\nSugar bear (sugar bearrrrr) ${handle}`
    T.post('statuses/update', { status })
        .then(val => console.log('Posted', val))
        .catch(err => console.error(err))
}

function searchForTweet() {
    return T.get('search/tweets', { q: 'from:@DammitOclock saved my life tonight', count: 100 })
}

async function hasTweetedToday() {
    try {
        const val = await searchForTweet();
        if (val.data.statuses) {
            let sameDay = false
            val.data.statuses.forEach(element => {
                sameDay = dayjs(element.created_at).utc().isSame(dayjs().utc(), 'date')
            });
            return sameDay;
        } else {
            return false;
        }
    } catch (error) {
        console.error(error);
        return false;
    }
}

module.exports = { sendTweet, saveLife, searchForTweet, hasTweetedToday }
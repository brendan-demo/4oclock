const express = require('express');
const twitter = require('./twitter');
const phrases = require('./phrases');

api = express.Router();

api.get('/', function (req, res) {
    res.json({ message: phrases.all[Math.floor(Math.random() * phrases.all.length)] })
})

api.get('/tweet', function (req, res) {
    try {
        twitter.sendTweet();
        res.json({ message: 'Sent' })
    } catch (error) {
        console.error(error);
        res.status(500).json(error);
    }
})

api.get('/savelife', async function (req, res) {
    try {
        let today = await twitter.hasTweetedToday()
        if (today) {
            res.status(418).json({ message: 'Someone already saved my life tonight' })
        } else {
            twitter.saveLife(true, req.query.handle);
            res.json({ message: 'Sent' })
        }
    } catch (error) {
        console.error(error);
        res.status(500).json(error);
    }
})

module.exports = api;
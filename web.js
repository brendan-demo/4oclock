const express = require('express')
const app = express()
const hbs = require('express-hbs');
const port = process.env.PORT || 3000

app.engine('hbs', hbs.express4({
  partialsDir: __dirname + '/views/partials',
  layoutsDir: __dirname + '/layouts',
  defaultLayout: __dirname + '/layouts/default.hbs'
}));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.use(express.static('static'))

// Sub routers
app.use('/api', require('./lib/api'))

app.get('/', (req, res) => {
  res.render('home');
})

app.listen(port, () => {
  console.log(`Butterflies are free to fly at http://localhost:${port}`)
})